; square value
(define (square x) (* x x))

; absolute value
(define (abs x)
  (if (< x 0)
    (- x)
    x))

; test if the guess is good enough (i.e., its square differs less than
; 0.001 to the radicand)
(define (good-enough? guess prev x) 
  (if (< (abs (- prev guess)) 0.01)
  (< (abs (- (square guess) x)) 0.01) #f))

; average guess with radicand/guess
(define (average guess x)
  (/ (+ (/ x guess) guess) 2))

; improve guess if it's not good enough
(define (improve guess x)
  (average guess x))

; iteration for computation of the square root
(define (sqrt-iter guess prev x)
  (if (good-enough? guess prev x)
    guess
    (sqrt-iter (improve guess x) guess x)))

; square root algorithm
(define (sqrt x)
  (sqrt-iter 1.0 0.0 x))
